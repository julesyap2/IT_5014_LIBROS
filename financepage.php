<?php include_once 'navbar.php'?>
<html>

<head>
    <?php include 'links.php'?>
    <script src='js/finance.js'> </script>
</head>
<style>
<?php include 'links.php'?>
</style>
<body>
<div class='table-container'>
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Overdue Fee</th>
                <th>Status</th>
                <th>Options</th>
            </tr>
        </thead>
        <tbody>
            <?php include('config.php');
            foreach ($users as &$user){
                if($user->type!=NULL && $user->type === "student"){
                    if($user->borrowStatus==="blocked"){
                        Echo "<tr id='".$user->_id."'>
                        <td>$user->_id</td>
                        <td>".$user->name->firstname."  ".$user->name->lastname."</td>
                        <td> 0 </td>
                        <td>$user->borrowStatus </td>
                        <td><form id='unblockUser'><input id='id' type='text' hidden value='".$user->_id."'><input type='submit' class='btn btn-primary' value='unblock'></form></td>
                        </tr>";
                    }
                } 
            }
            ?>
           
        </tbody>
    </table>
    </div>
</body>

</html>

<script>
    $(document).ready(function() {
        var table  = $('#example').DataTable();
    });
</script>