<html>
<title>LIBROS</title>

<head>
    <?php include 'navbar_links.php'  ?>
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-light" id='nav'>
    <img src="package/build/svg/book.svg" alt="Smiley face"> <a class="navbar-brand" href="#">&nbsp;LIBROS</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>


    <div class="collapse navbar-collapse" id="navbarColor03">
        <ul class="navbar-nav mr-auto">

            <!-- HOME -->
            <li class="nav-item active">
                <a class="nav-link" id="home" href="inventoryStudents.php">Home <span class="sr-only">(current)</span></a>
            </li>

            <!-- RESERVATION -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Reservation
          </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Borrows Left</a>
                    <a class="dropdown-item" href="Cart.php">Create Reservation</a>
                </div>
            </li>

            <!-- BOOK PREVIEW -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Book Preview
          </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#"><img src="package/build/svg/file.svg" height="15px" width="15px">&nbsp;&nbsp;View Details</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#"><img src="package/build/svg/book.svg" height="15px" width="15px">&nbsp;&nbsp;Check Available Books</a>
                </div>
            </li>

            <!-- PROFILE -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Profile
          </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                    <a class="dropdown-item" href="StudentProfile.php"><img src="package/build/svg/person.svg" height="15px" width="15px">&nbsp;&nbsp;Account Information</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#"><img src="package/build/svg/globe.svg" height="15px" width="15px">&nbsp;&nbsp;Borrow History</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#"><img src="package/build/svg/archive.svg" height="15px" width="15px">&nbsp;&nbsp;Unsettled Fines</a>
                    <div class="dropdown-divider"></div>

                    <a class="dropdown-item" href="inventoryStudents.php"><img src="package/build/svg/book.svg" height="15px" width="15px">&nbsp;&nbsp;Currently Borrowed Books</a>
                    <div class="dropdown-divider"></div>

                    <a class="dropdown-item" href="#"><img src="package/build/svg/report.svg" height="15px" width="15px">&nbsp;&nbsp;Reservation Status</a>
                </div>
            </li>
        </ul>
        <!--          <form class="form-inline" id='searchcontainer'>
            <input class="form-control" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success" id='search' type="submit">Search</button>
          </form> -->
    </div>
</nav>

</body>

</html>
